package eu.wiktordolecki.corporation;

import eu.wiktordolecki.corporation.model.CharacterData;
import eu.wiktordolecki.corporation.pdf.CharacterPdfCreator;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;

import static org.springframework.http.MediaType.*;

@RestController
@CrossOrigin(origins = {"http://localhost:9000", "https://stmi.gitlab.io"})
public class CharacterController {

    @PostMapping(value = "/createCharacter", consumes = "application/json", produces = APPLICATION_JSON_UTF8_VALUE)
    public CharacterData createCharacter(
            @RequestBody CharacterData characterData) {
        return characterData;
    }

    @PostMapping(value = "/printCharacter", produces = APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> printCharacter(
            @RequestBody CharacterData characterData
    ) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        CharacterPdfCreator pdfCreator = new CharacterPdfCreator();
        byte[] bytes = pdfCreator.printCharacterToByteArray(characterData);

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(bytes.length)
                .contentType(APPLICATION_PDF)
                .body(new InputStreamResource(new ByteArrayInputStream(bytes)));
    }
}
