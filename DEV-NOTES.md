# Local HTTP Server to serve page

```
> cd corporation-char/page/
> python -m SimpleHTTPServer 9000
```

# Download links

* Homepage http://danml.com/download.html

* github https://github.com/rndme/download

# Spring Guides links

* CORS configuration https://spring.io/guides/gs/rest-service-cors/

# GitLab CI reference

* Deploying to Cloud Foundry from GitLab CI https://docs.gitlab.com/ee/ci/examples/deploy_spring_boot_to_cloud_foundry/

# Cloud Foundry/Pivotal manifest.yml

* Dev Guide on deployment https://docs.run.pivotal.io/buildpacks/java/getting-started-deploying-apps/gsg-spring.html

* Manifest reference https://docs.pivotal.io/pivotalcf/2-6/devguide/deploy-apps/manifest.html

* Java buildpack tips https://docs.pivotal.io/pivotalcf/2-6/buildpacks/java/java-tips.html

* Reference of java buildpack memory calculator https://github.com/cloudfoundry/java-buildpack-memory-calculator