# Backend Development H Demo Session

* Tilte: Using GitLab CI as code for hobby and professional projects
* Date: Friday 2019-09-20

## Intro

Before discussing GitLab CI let's first quickly tour building blocks used:

1. Spring 5 Framework
  * Spring Guides https://spring.io/guides/
  * Spring Initalizr https://start.spring.io/
  * Spring-Boot Maven Plugin https://docs.spring.io/spring-boot/docs/2.1.8.RELEASE/maven-plugin/
  
2. Vue.js Framework for frontend
  * Framework Documentation https://vuejs.org/v2/guide/
  * Video Courses https://www.vuemastery.com/courses/
  
3. Cloud Foundry and Pivotal Web Services
  * Cloud Foundry https://www.cloudfoundry.org/
  * Pivotal Web Services https://run.pivotal.io/

4. Docker and Docker Hub, Alpine Linux and build/deploy tooling
  * Docker Container Software https://www.docker.com/
  * Docker Hub for reviewed container images https://hub.docker.com/
  * Alpine Linux and docker image https://alpinelinux.org
  * OpenJDK https://openjdk.java.net/
  * Apache Maven https://maven.apache.org/
  * Cloud Foundry CLI https://docs.cloudfoundry.org/cf-cli/
  
## GitLab CI as code

1. Definition in code at .gitlab-ci.yml file
2. Jobs and Pipelines
3. Structure of Job
4. GitLab pages free hosting of static web pages

## Live Demo

1. Short discussion of demo app structure
2. GitLab Web IDE
3. Pipline in action demonstration
4. Quota and Security

## End Notes

1. GitHub alternative - CircleCI https://github.com/marketplace/circleci
2. Kubernetes Credits for Google Cloud 
3. MongoDB Atlas https://cloud.mongodb.com
4. Auth0 https://auth0.com